user_pref("privacy.resistFingerprinting", true);

// Hardware Video Acceleration
user_pref("media.ffmpeg.vaapi.enabled", true);
// Hardware video decoding
user_pref("media.hardware-video-decoding.force-enabled", true);

// Disable disk-cache
user_pref("browser.cache.disk.enable", false);
user_pref("browser.cache.memory.enable", true);
user_pref("browser.cache.disk.amount_written", 312448);

// Alternative: disk cache on RAM (not used)
//user_pref("browser.cache.disk.parent_directory", "/run/user/1000/firefox");