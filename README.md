# My Custom Firefox

A small build system for generating Firefox "profiles" using [Policies](https://github.com/mozilla/policy-templates/blob/master/README.md) and [Autoconfig](https://support.mozilla.org/en-US/kb/customizing-firefox-using-autoconfig), both focused on hardening.

Each one of bundled "profiles" are supposed to be deployed and used on different devices. By default, two "profiles" are included:

- Personal
- Work

>Let's say "Halli Hallo!" to renewable Firefox profiles.

Note: this solution is focused to machines running **Arch Linux**.

You should use this project as inspiration for covering your own needs, as this is focused on my personal requirements and they might not be compatible at all.

# Requirements

- Make
- Python 3.9 or above
- `python-pyaml` module at [Arch Pkg](https://archlinux.org/packages/community/any/python-pyaml/)
- Firefox

# Steps

## BACKUP YOUR IMPORTANT STUFF

**BACKUP YOUR IMPORTANT STUFF FROM ANY EXISTING FIREFOX INSTALLATIONS: bookmarks, configurations, passwords or even the whole profile**

## Create your own "profile" (optional)

You should do this only if you intend to customize things.

1. Create your custom policies profile in YAML format at `policies-per-profile` folder.
1. Create your custom settings for Autoconfig at `settings` folder.
1. Add a rule to generate the output files into the Makefile

## Build and distribute files

1. Simply run `make <profile_name>`. Valid options are `make personal` and `make work`
1. Generated files will be available at `output` folder
1. Feel free to clean the house when finished, using `make clean`

### Manual steps

1. Copy the generated `output/policies.json` to `/etc/firefox/policies/`. Remember this solution is focused to Archlinux installations and the path might be a bit different for your distro.
1. Create a new Firefox profile, run `firefox -no-remote -P` for launching the Profile assistant
1. Copy the generated `output/user.js` to the root of your new Firefox profile. It should be located at `~/.mozilla/firefox/<your_firefox_profile>`
1. Launch Firefox with your new profile: the extensions on the generated files will be installed automatically on the first run and the custom configuration will also be applied automatically. Let it work by itself for 2 minutes
1. (Optional): You're almost ready: feel free to configure each extension/add-on. You could import the settings stored at `addons-exported-settings` folder, where you can also save configuration from add-ons that allow exporting its config.
1. (Optional): Change your search engine to Duckduckgo
1. (Optional): Ensure the extensions are allowed to run under Private Browsing
1. (Optional): Import your personal bookmarks to the new profile
1. Confirm there aren't any errors at the `about:policies` page and your settings are available at `about:config`
1. Enjoy!

# Important notes

## You should only use one "profile" per machine

This is a limitation from the Policies file, as it must be stored under a system folder. For that reason, you should keep the Policies file as small as possible and any further customizations should be defined at the `user.js` file.

## About Settings Storage (user.js and equivalent)

The user.js solution is used by Firefox to store configurations managed at `about:config`. This file is tied to a particular Firefox profile, but there are workarounds to apply those settings globally.

Currently my needs don't care if the settings are widely applied to all profiles, as I'm only using only a couple of profiles

# For advanced users

## Why are you using both Policies and Settings Storage?

Because Firefox has a mess (desmadre) and the configuration management is hard to understand. I'll try to keep a balance between both features that are easy to understand, but focusing as much as possible on the hardening & privacy of the resulting Firefox profile.

## Adding a new extension

For adding a new Firefox extension to your custom "profile", go to the **Add-ons** section through `about:support` and find its ID in the format `something@something-else`. Many extensions have a weird name, like an UUID, so don't be confused, you can add them anyways.

## Merging vs replacing policies

By default, any policy found at your custom policy file will replace the values found at the `policies-global.yaml`. The exceptions are for a few special policies defined at `CONST_POLICIES_TO_MERGE` collection in the `generate-policy.py` script where the values from the custom profile will be merged with the existing at the base profile.

## Hardening

Take a look to the projects mentioned at [Firefox's Arch Wiki page](https://wiki.archlinux.org/title/Firefox/Privacy#Hardened_user.js_templates)

# License

MIT