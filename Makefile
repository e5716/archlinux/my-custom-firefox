PROFILEPREFIX_PERSONAL := personal
PROFILEPREFIX_WORK := work

OUTPUT_DIR := ./output
OUTPUT_PREFS_FILE := user.js

personal:
	python generate-policy.py --policies-file $(PROFILEPREFIX_PERSONAL).yaml
	cat settings-global.js settings/$(PROFILEPREFIX_PERSONAL).js > $(OUTPUT_DIR)/$(OUTPUT_PREFS_FILE)
work:
	python generate-policy.py --policies-file $(PROFILEPREFIX_WORK).yaml
	cat settings-global.js settings/$(PROFILEPREFIX_WORK).js > $(OUTPUT_DIR)/$(OUTPUT_PREFS_FILE)
clean:
	rm -rf $(OUTPUT_DIR)