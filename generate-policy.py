#!/usr/bin/env python

import argparse
import json
import os
import yaml
from collections import OrderedDict

# Notes
# - This script has some flexibility, it won't verify whether the custom profile policies has valid policies, but an empty file should exist instead to generate a policies.json with only the base rules (not tested yet)

# Constants
# Note: all paths are relative to the location of this script (CWD)
CONST_CUSTOM_POLICIES_DIRECTORY = 'policies-per-profile'
CONST_BASE_POLICIES_FILE = 'policies-global.yaml'
CONST_OUTPUT_FOLDER = 'output'
CONST_OUTPUT_FILE = 'policies.json' # It should be placed at /etc/firefox/policies/ and it will become a system-wide policy. This does fit my needs currently.
# By default, the policies defined at the custom policies profile will override the policies found on the base file, unless specified on this set. Note: this is a basic functionality and it will only work with policies on the first level of the root, i.e., 'policies.ExtensionSettings', 'policies.Preferences' and similar policies that allow an unlimited number of children as keys. All these mergeable policies are represented as a dictionary in Python; it's useless to merge primitive datatypes
CONST_POLICIES_TO_MERGE = set([
    'ExtensionSettings',
    'Preferences',
    'ManagedBookmarks'
])

class PolicyException(Exception):
    pass

class SourceBuilder(object):
    _instance = None
    _basePolicies = None
    _customProfilePolicies = None

    def __new__(cls):
        if cls._instance is None:
            cls._instance = super(SourceBuilder, cls).__new__(cls)
        return cls._instance

    def _getParsedObjectFromFile(cls, sourceFile):
        # 'with' statement handles automatically the file closing
        with open(sourceFile, 'r') as fileInstance:
            parsedYaml = yaml.load(fileInstance.read(), Loader = yaml.FullLoader) # Source: https://github.com/yaml/pyyaml/wiki/PyYAML-yaml.load(input)-Deprecation
        return parsedYaml

    def _handleFilesReading(cls, customPoliciesFile):
        cls._basePolicies = cls._getParsedObjectFromFile(CONST_BASE_POLICIES_FILE)
        cls._customProfilePolicies = cls._getParsedObjectFromFile(CONST_CUSTOM_POLICIES_DIRECTORY + '/' + customPoliciesFile)

    def _validateYamlContents(cls):
        if cls._basePolicies is None:
            raise PolicyException('Base policies source file is empty')
        elif cls._customProfilePolicies is None:
            raise PolicyException('Custom policies source file is empty')

    def readCustomProfilePolicies(cls, customPoliciesFile):
        cls._handleFilesReading(customPoliciesFile)
        cls._validateYamlContents()
        return cls._instance
    
    def unifyPolicies(cls): # Merge and replace
        policiesFromBaseSource = cls._basePolicies
        policiesFromCustomSource = cls._customProfilePolicies
        for customPolicy in policiesFromCustomSource.keys():
            # Merging custom policy into the base policies
            if customPolicy in CONST_POLICIES_TO_MERGE and customPolicy in policiesFromBaseSource:
                policiesFromBaseSource[customPolicy] = policiesFromBaseSource[customPolicy] | policiesFromCustomSource[customPolicy] # The values from the custom source will have precedence over the base source. See "|" operator docs https://docs.python.org/3.9/library/stdtypes.html#dict.values
            
            # Replacing or adding (if not exists) custom policy into the base policies
            else:
                policiesFromBaseSource[customPolicy] = policiesFromCustomSource[customPolicy]

        return cls._instance

    def getResults(cls):
        # The resulting JSON file must have a root key called "policies". Also, the keys are sorted alphabetically for a better reading of the resulting file
        return {'policies': OrderedDict(sorted(cls._basePolicies.items()))}

parser = argparse.ArgumentParser(description='Generates a policies.json file used for Firefox policies from a YAML-based policies file')
parser.add_argument('-p', '--policies-file', dest='customPoliciesYamlFile', type=str, required=True, help='Name of the custom policies file with the extension: .yaml or .yml')
args = parser.parse_args()

try:
    if not os.path.exists(CONST_OUTPUT_FOLDER):
        os.makedirs(CONST_OUTPUT_FOLDER)

    with open(CONST_OUTPUT_FOLDER + '/' + CONST_OUTPUT_FILE, 'w') as fileInstance:
        json.dump(SourceBuilder().readCustomProfilePolicies(args.customPoliciesYamlFile).unifyPolicies().getResults(), fileInstance, ensure_ascii=True, indent=4)
except Exception as e:
    print('Error: ' + str(e))
    exit(1)

# TODO: backup current firefox and test. Ensure you're running Wayland AND hardware accelaration, webrender https://wiki.archlinux.org/title/Firefox/Tweaks, ensure general.platform.override is overwritten with the change user agent extension and privacy.fingerprint
# revisar bookmarks y extensiones, traer bookmarks jale desde la laptop trabajo